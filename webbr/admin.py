from django.contrib import admin
from .models import Topic, Sentiment, TrendingIndia, TrendingWorldwide

admin.site.register(Topic)
admin.site.register(Sentiment)
admin.site.register(TrendingIndia)
admin.site.register(TrendingWorldwide)

