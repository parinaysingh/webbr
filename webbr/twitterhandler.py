from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import json
import time


class Listener(StreamListener):

    def __init__(self, time_limit=120):
        self.start_time = time.time()
        self.limit = time_limit
        self.file = 'temporaryFiles/temp.txt'
        with open(self.file, 'w', encoding='utf8') as f:
            f.truncate()
        super(StreamListener, self).__init__()

    def on_data(self, data):
        try:
            all_data = json.loads(data)
            tweet = all_data["text"]
            with open(self.file, 'a', encoding='utf8') as f:
                f.write(tweet + "\n")
            if (time.time() - self.start_time) < self.limit:
                return True
            else:
                return False

        except Exception as e:
            print("Error on_data: " + str(e))
            if (time.time() - self.start_time) < self.limit:
                return True
            else:
                return False

    def on_error(self, status):
        print(status)
        return False

#Consumer Key (API Key)	I3w3Fi07pnwvQ8ZH1BxbuOV5D
#Consumer Secret (API Secret)	qpTlPn2FVHDjsZ6taoWUGLaUU0qvJ7aga0WWruW3nqxT6Y0Yxl
#Access Token	750011420461314049-j2MdCn4ZjD50yJhFNUrusXuqoUem2yP
#Access Token Secret	YTsrPMOKgD4j4Rk88cWzKE832RPSFx9tDVbEn0OlwZdGd

#consumer key, consumer secret, access token, access secret.
ckey="I3w3Fi07pnwvQ8ZH1BxbuOV5D"
csecret="qpTlPn2FVHDjsZ6taoWUGLaUU0qvJ7aga0WWruW3nqxT6Y0Yxl"
atoken="750011420461314049-j2MdCn4ZjD50yJhFNUrusXuqoUem2yP"
asecret="YTsrPMOKgD4j4Rk88cWzKE832RPSFx9tDVbEn0OlwZdGd"

auth = OAuthHandler(ckey, csecret)
auth.set_access_token(atoken, asecret)


def stream(keyword):
    print('Streaming...\n')
    twitter_stream = Stream(auth, Listener())
    twitter_stream.filter(languages=["en"], track=[keyword])
