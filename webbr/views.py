from django.shortcuts import render, get_object_or_404, redirect
from webbr.models import Topic, TrendingWorldwide, TrendingIndia
from .models import topics
from django.http import HttpResponse


def index(request):
    return render(request, 'webbr/index.html', {'topics': topics})


def topic_index(request, topic_name):
    all_topic_obj = Topic.objects.filter(topic=topic_name)
    if len(all_topic_obj) == 0:
        return redirect('webbr:index')
    sent = all_topic_obj[0].sentiment_set.all().order_by('date')
    name = all_topic_obj[0].display_name
    context = {'sent': sent, 'name': name, 'all_topic_obj': all_topic_obj, 'topic_name': topic_name}
    return render(request, 'webbr/detail.html', context)


def details(request, id):
    topic_obj = get_object_or_404(Topic, pk=id)
    topic_name = topic_obj.topic
    all_topic_obj = Topic.objects.filter(topic=topic_name)
    if len(all_topic_obj) == 0:
        return redirect('webbr:index')
    sent = topic_obj.sentiment_set.all().order_by('date')
    name = topic_obj.display_name
    context = {'sent': sent, 'name': name, 'all_topic_obj': all_topic_obj, 'topic_name': topic_name}
    return render(request, 'webbr/detail.html', context)


def trending(request):
    trending_india = TrendingIndia.objects.all()
    trending_world = TrendingWorldwide.objects.all()
    context = {'trending_india': trending_india, 'trending_world': trending_world, 'topic_name': 'trending'}
    return render(request, 'webbr/trending.html', context)
