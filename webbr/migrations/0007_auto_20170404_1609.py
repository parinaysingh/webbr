# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-04 10:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webbr', '0006_auto_20170404_1518'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='trending',
            name='topic',
        ),
        migrations.AddField(
            model_name='trending',
            name='keyword',
            field=models.CharField(default=2, max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='trending',
            name='volume',
            field=models.IntegerField(default=0),
        ),
    ]
