from django.conf.urls import url
from . import views

app_name = 'webbr'

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^trending/$', views.trending, name="trending"),
    url(r'^(?P<topic_name>[a-z]+)/$', views.topic_index, name="topic_index"),
    url(r'^details/(?P<id>[0-9]+)/$', views.details, name="details"),
]