import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "wproject.settings")
django.setup()
###-####

import datetime
from webbr.models import TrendingIndia, TrendingWorldwide
import twitterhandler as handler
import sentiment_analysis as analyzer


def get_sentiment_value(class_name):
    for s in class_name.objects.filter(date=str(datetime.date.today()))[:2]:
        if s.value == 0:
            handler.stream(s.keyword)
            s.value = analyzer.sentiment()
            s.save()
            print("Sentiment Intensity : ", s, ' :', s.value)

get_sentiment_value(TrendingIndia)
get_sentiment_value(TrendingWorldwide)



