from django.db import models
import datetime

topics = ['politics', 'movies', 'music']


class Topic(models.Model):
    topic = models.CharField(max_length=10, choices=[(name, name) for name in topics])
    keywords = models.CharField(max_length=50)
    display_name = models.CharField(max_length=30)

    def __str__(self):
        return self.display_name + ' - ' + self.topic


class Sentiment(models.Model):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    date = models.CharField(default=str(datetime.date.today()), max_length=10)
    value = models.FloatField(default=0)

    def __str__(self):
        return self.topic.display_name + ' ' + self.date


class Trending(models.Model):
    keyword = models.CharField(max_length=50)
    date = models.CharField(default=str(datetime.date.today()), max_length=10)
    value = models.FloatField(default=0)

    def __str__(self):
        return self.keyword + ' ' + self.date


class TrendingIndia(Trending):
    def __str__(self):
        return self.keyword + ' ' + self.date


class TrendingWorldwide(Trending):
    def __str__(self):
        return self.keyword + ' ' + self.date
