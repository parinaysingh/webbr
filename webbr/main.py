import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "wproject.settings")
django.setup()
###-####

import datetime
from webbr.models import Topic, Sentiment
import twitterhandler as handler
import sentiment_analysis as analyzer

all_topics = Topic.objects.all()
curr_date = str(datetime.date.today())
for curr_topic in all_topics:
    sent = Sentiment.objects.filter(topic=curr_topic, date=curr_date)
    if len(sent) == 0:
        print(curr_topic.display_name)
        keyword = curr_topic.keywords
        handler.stream(keyword)
        sentiment_value = analyzer.sentiment()
        sentiment = Sentiment()
        sentiment.topic = curr_topic
        sentiment.value = sentiment_value
        sentiment.save()
        print(sentiment_value)
