data = document.getElementById("data");
name = document.getElementById("name").innerHTML;
a = data.getElementsByTagName("p");
data_arr = [];
for (i = 0; i < a.length; i++) {
    arr = a[i].innerHTML;
    splitted_data = arr.split(" ");
    date = splitted_data[0].split("-");
    sent_value = splitted_data[1];
    data_arr.push([
        Date.UTC(parseInt(date[0]), parseInt(date[1]) - 1, parseInt(date[2])),
        parseFloat(sent_value)
    ]);
}

Highcharts.Tick.prototype.drillable = function () {};

Highcharts.chart('container', {
    chart: {
        type: 'spline'
    },
    exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    },
    title: {
        text: name
    },
    subtitle: {
        text: 'Sentiment Analysis'
    },
    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
            month: '%e. %b',
            year: '%b'
        },
        title: {
            text: 'Date'
        }
    },
    yAxis: {
        title: {
            text: 'Sentiment'
        }
    },
    plotOptions: {
        spline: {
            marker: {
                enabled: true
            }
        }
    },
    series: [{
        name: 'Sentiment Value',
        data: data_arr
    }]
});