import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "wproject.settings")
django.setup()
###-####
import tweepy
import datetime
from operator import itemgetter
from webbr.models import TrendingWorldwide, TrendingIndia

ckey="I3w3Fi07pnwvQ8ZH1BxbuOV5D"
csecret="qpTlPn2FVHDjsZ6taoWUGLaUU0qvJ7aga0WWruW3nqxT6Y0Yxl"
atoken="750011420461314049-j2MdCn4ZjD50yJhFNUrusXuqoUem2yP"
asecret="YTsrPMOKgD4j4Rk88cWzKE832RPSFx9tDVbEn0OlwZdGd"

auth = tweepy.OAuthHandler(ckey, csecret)
auth.set_access_token(atoken, asecret)
api = tweepy.API(auth)
'''
country_woeid = []

data = api.trends_available()
for w in data:
    app = (w['country'], w['woeid'])
    country_woeid.append(app)

d = sorted(country_woeid, key=itemgetter(0))
a = []

for w in d:
    if w[0] == 'India':
        a.append(w[1])

print(a)

#[2282863, 2295377, 2295378, 2295381, 2295383, 2295386, 2295387, 2295388, 2295401, 2295402, 2295404, 2295405, 2295407, 2295408, 2295410, 2295411, 2295412, 2295414, 2295420, 2295424, 20070458, 23424848]
final = []

prev = "a"
for w in d:
    if prev == w[0]:
        pass
    else:
        final.append(w)
        prev = w[0]

print(final)
'''


def get_trending(woeid_arr):
    data_prep = []
    for woeid in woeid_arr:
        print(woeid)
        data = api.trends_place(woeid)
        for w in data[0]['trends']:
            if w['tweet_volume'] is None:
                w['tweet_volume'] = 0
            app = (w['name'], w['query'], w['tweet_volume'])
            data_prep.append(app)
    data_sort = sorted(data_prep, key=itemgetter(2), reverse=True)
    prev = '.'
    trending = []
    for w in data_sort:
        if w[1] != prev:
            trending.append(w)
            prev = w[1]
    return trending[:10]

curr_date = datetime.date.today()
tredn_objs = TrendingIndia().objects.filter(date=curr_date)

woeid_india = [2282863, 2295377, 2295378, 2295381, 2295383, 2295386, 2295387, 2295388, 2295401, 2295402, 2295404, 2295405, 2295407, 2295408, 2295410, 2295411, 2295412, 2295414, 2295420, 2295424, 20070458, 23424848]
woeid_worldwide = [1]

trending_india = get_trending(woeid_india)
trending_worldwide = get_trending(woeid_worldwide)

for w in trending_india:
    inst = TrendingIndia()
    inst.keyword = w[0]
    inst.volume = int(w[2])
    inst.save()

for w in trending_worldwide:
    inst = TrendingWorldwide()
    inst.keyword = w[0]
    inst.save()
