import math
import nltk
import pickle
from nltk.tokenize.casual import TweetTokenizer


save_lexicons = open("pickle/normalized_lexicons.pickle", "rb")
lexicons = pickle.load(save_lexicons)
save_lexicons.close()

save_rev_polarity = open("pickle/rev_polarity_words.pickle", "rb")
rev_polarity = pickle.load(save_rev_polarity)
save_rev_polarity.close()

save_increase_polarity = open("pickle/increase_polarity.pickle", "rb")
increase_polarity = pickle.load(save_increase_polarity)
save_increase_polarity.close()

save_decrease_polarity = open("pickle/decrease_polarity.pickle", "rb")
decrease_polarity = pickle.load(save_decrease_polarity)
save_decrease_polarity.close()

save_stopwords = open("pickle/stopwords.pickle", "rb")
stop_words = pickle.load(save_stopwords)
save_stopwords.close()

tokenizer = TweetTokenizer(strip_handles=True, reduce_len=True)
posemojis = "😂😄😃😀😊😉😍😘😚😗😙😜😝😛😁😅😆😋😎😈😇☺😏🙃🤓🙂🤗🤣🤠🤤👶💛🙎👰🤳🕺🙋👸👼💙💜💚❤💗💓💕💖💞💘💝💌💋😺😸😻😽😼😹👻☃"
negemojis = "😔😌😒😖😡😠😱😨😫😩😓😰😥😭😢😣😞😷😵😟😦😧👿😕☹🤒🤕🙁🤥🤢🤧🤦🙅💔😿😾"


def sentiment_score(all_tweets):
    total_sum = 0
    total_norm_len = 0
    for tweet in all_tweets:
        sum = 0
        flag = 0
        for emoji in posemojis:
            if tweet.__contains__(emoji):
                sum = 20
                flag = 1
        for emoji in negemojis:
            if tweet.__contains__(emoji):
                sum = -20
                flag = 1

        if not flag:
            tweet = tweet.lower()
            all_sentences = nltk.sent_tokenize(tweet)
            for sent in all_sentences:
                prev = ''
                words = tokenizer.tokenize(sent)
                for w in words:
                    if w == "of" and prev == "kind":
                        sum -= lexicons["kind"]
                        prev = "kindof"

                    if w not in stop_words:
                        if w in lexicons:
                            if prev in rev_polarity:
                                sum -= lexicons[w]
                            else:
                                sum += lexicons[w]

                            if prev in increase_polarity:
                                if lexicons[w] > 0:
                                    sum += 0.5 * lexicons[w]
                                else:
                                    sum -= 0.5 * lexicons[w]

                            if prev in decrease_polarity:
                                if lexicons[w] < 0:
                                    sum += 0.5 * lexicons[w]
                                else:
                                    sum -= 0.5 * lexicons[w]
                        prev = w
        if sum != 0:
            print(tweet, sum)
            total_sum += sent_normalize(sum)
            total_norm_len += 1

    total_sum /= total_norm_len
    return total_sum


def sent_normalize(sent_value):
    return sent_value/math.sqrt((sent_value*sent_value) + 1)


def sentiment():
    print("Analyzing...\n")
    wfp = open("temporaryFiles/temp.txt", encoding='utf8')
    doc = wfp.read()
    wfp.close()
    all_tweets = []
    for w in doc.split("\n"):
        all_tweets.append(w)
    grand_sum = sentiment_score(all_tweets)
    return grand_sum
