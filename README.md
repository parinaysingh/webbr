# README #

A Website for Sentiment Analysis on Python & Django.

Webbr understands the emotions people use in their online communication and translates them into computer language. This data can be used to gain a deeper understanding of the world, by showing you how people across the world feel about various topics, such as companies, politics, war and much more.

Using rule-based approach, Sentiment analysis is carried out by the system on the tweets fetched from twitter. A graph plotting analysis history is plotted.

![](previewImages/home.JPG)
![](previewImages/details.JPG)
![](previewImages/trending.JPG)
![](previewImages/mob_home.JPG)
![](previewImages/mob_details.JPG)
![](previewImages/mob_trending.JPG)